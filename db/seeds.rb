# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

100.times do |_index|
  Event.create!(
    name: Faker::Verb.simple_present,
    date: Faker::Date.between(2.days.ago, 1.month.from_now),
    description: Faker::Alphanumeric.alpha(15),
    email: Faker::Internet.email
  )
end
p "Created #{Event.count} events"
