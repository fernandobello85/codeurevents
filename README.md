# README

Cette application web est développée sur Ruby et RubyOnRails en utilisant PostgreSQL comme SGBD.
Bootstrap pour le Front-End.

---
## Information technique:

* Ruby version : ruby '2.5.1'

* Rails version : 'rails', '~> 5.2.3'

* DB: PostgreSQL RDBMS

---

# Test technique
## Codeur-Events App

Cette application web de deux niveaux, contient trois pages principales:

* Home
    * Create Event
    * Events

Additionnellement elle a deux pages pour la gestion des événements :

* Show Event. Permet d’afficher les informations d’un événement en particulière.
* Edit Event. Permet de modifier toutes les informations d’un événement.

Pour ajouter un nouveau événement l’utilisateur devra saisir :

* Le nom de l’événement
* La date de l’événement
* Une description de maximum 300 caractères
* Le mail de l’organisateur

*Tous les champs sont obligatoires et doivent être valides.*

Pour la liste d’événements :
* Le tableau affiche toutes les caractéristiques des événements.
* Les événements dons la date est dans les 10 prochains jours, le titre ou nom est affiché en rouge.
* Les événements dont la date est déjà passée ne sont pas affichés.
* Les événements peuvent être triés par date de création et par date de réalisation de l’événement.

*L’application permet de créer des événements avec une date antérieure. Cela afin de tester la fonction de filtrage des événements passés.*

*Un seed est crée afin de populer la BD*

**L'application Codeur-Events est hébergée sur une instance de Heroku**

[Codeur-Event](https://codeurevents.herokuapp.com/)

---
## Commands d'execution:

~~~sh
bundle

rake db:setup

rails db:drop db:create db:migrate db:seed

rails s
Testing...
~~~

