# frozen_string_literal: true

class Event < ApplicationRecord
  validates :name, presence: true
  validates :date, presence: true
  validates :description, presence: true, length: { maximum: 300 }
  validates :email, presence: true
  scope :get_coming, -> { where('date >= ?', Date.today) }
end
